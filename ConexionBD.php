<?php

//intenta conectar al sistema de bases de datos usando la función mysqli_connect() 
//especificando la dirección del sevidor, el usuario, la contraseña y la base de datos. 
//La dirección del servidor es 127.0.0.1 ó localhost, 
//ya que los servidores web y de bases de datos están en la misma máquina.

$enlace = mysqli_connect("127.0.0.1", "lamp_user", "1234", "lamp_db");

if (!$enlace) {
        echo "Error: No se pudo conectar a MariaDB/MySQL.\n";
        echo "código de error: " . mysqli_connect_errno() . PHP_EOL;
        echo "mensaje de error: " . mysqli_connect_error() . PHP_EOL;
        exit;
}

echo "<h1>Éxito: ¡Se realizó una conexión apropiada a MariaDB/MySQL!</h1>\n";
echo "<h2>Información del host: " . mysqli_get_host_info($enlace) . "</h2>\n";
mysqli_close($enlace);
>?